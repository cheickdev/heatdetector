import com.cg.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Dichotomy dichotomy = new PlanDichotomy(new PlanMagic());
        System.out.println(dichotomy.findMagic());
        dichotomy = new LinearDichotomy(new LinearMagic());
        System.out.println(dichotomy.findMagic());
        dichotomy = new PlanDichotomyString(new PlanMagicString());
        System.out.println(dichotomy.findMagic());
    }
}
