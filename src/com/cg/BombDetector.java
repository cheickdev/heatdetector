package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public class BombDetector {
    String direction;
    Point a;
    Point b;
    Point batman;

    public Point findNext() {
        Point position = Point.createPositionFromDirection(direction);
        if (position.x < 0) {
            b.x = batman.x;
        }
        if (position.x > 0) {
            a.x = batman.x;
        }
        if (position.y < 0) {
            b.y = batman.y;
        }
        if (position.y > 0) {
            a.y = batman.y;
        }
        batman = a.mean(b);
        return batman;
    }
}
