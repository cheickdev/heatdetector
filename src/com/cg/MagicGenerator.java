package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public interface MagicGenerator<T> {
    T generate();
}
