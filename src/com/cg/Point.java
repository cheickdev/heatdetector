package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public class Point implements Comparable<Point> {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isNull() {
        return x == 0 && y == 0;
    }

    public static Point createPositionFromDirection(String dir) {
        int x = 0;
        int y = 0;
        if (dir.contains("U")) {
            y = -1;
        }
        if (dir.contains("R")) {
            x = 1;
        }
        if (dir.contains("D")) {
            y = 1;
        }
        if (dir.contains("L")) {
            x = -1;
        }
        return new Point(x, y);
    }

    public String getDirection() {
        String dir = "";
        if (y < 0) {
            dir += "U";
        }
        if (x > 0) {
            dir += "R";
        }
        if (y > 0) {
            dir += "D";
        }
        if (x < 0) {
            dir += "L";
        }
        return dir;
    }

    @Override
    public int compareTo(Point o) {
        if (x < o.x) {
            return -1;
        }
        if (x > o.x) {
            return 1;
        }
        if (y < o.y) {
            return -1;
        }
        if (y > o.y) {
            return 1;
        }
        return 0;
    }

    public Point mean(Point p) {
        return new Point((x + p.x) / 2, (y + p.y) / 2);
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}
