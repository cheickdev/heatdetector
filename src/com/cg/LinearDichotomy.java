package com.cg;

import java.util.Random;

/**
 * Created by sissoko on 15/04/2016.
 */
public class LinearDichotomy extends Dichotomy<Integer> {

    public LinearDichotomy(Magic magic) {
        super(magic);
    }

    public Integer findMagic() {
        int a = 0;
        int b = LinearMagic.MAX_VALUE;
        int geussed = new Random().nextInt(b);
        int position = (int) magic.guess(geussed);
        while (position != 0) {
            if(position < 0) {
                b = geussed;
            }
            if(position > 0) {
                a = geussed;
            }
            geussed = (a + b) / 2;
            position = (int) magic.guess(geussed);
        }
        return geussed;
    }
}
