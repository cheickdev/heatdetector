package com.cg;

import java.util.Scanner;

/**
 * Created by sissoko on 15/04/2016.
 */
public class Player {

    static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static Point createPositionFromDirection(String dir) {
            int x = 0;
            int y = 0;
            if (dir.contains("U")) {
                y = -1;
            }
            if (dir.contains("R")) {
                x = 1;
            }
            if (dir.contains("D")) {
                y = 1;
            }
            if (dir.contains("L")) {
                x = -1;
            }
            return new Point(x, y);
        }

        public Point mean(Point p) {
            return new Point((x + p.x) / 2, (y + p.y) / 2);
        }

        @Override
        public String toString() {
            return x + " " + y;
        }
    }

    static class BombDetector {
        String direction;
        Point a;
        Point b;
        Point batman;
        int tries;

        public Point findNext() {
            if (--tries <= 0) {
                System.err.println("Maximum guess exceed...");
                return null;
            }
            Point position = Point.createPositionFromDirection(direction);
            if (position.x < 0) {
                b.x = batman.x;
            }
            if (position.x > 0) {
                a.x = batman.x;
            }
            if (position.y < 0) {
                b.y = batman.y;
            }
            if (position.y > 0) {
                a.y = batman.y;
            }
            batman = a.mean(b);
            return batman;
        }
    }

    static BombDetector detector = new BombDetector();
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        detector.a = new Point(0, 0);
        detector.b = new Point(in.nextInt(), in.nextInt());
        detector.tries = in.nextInt(); // maximum number of turns before game over.
        detector.batman = new Point(in.nextInt(), in.nextInt());
        // game loop
        while (true) {
            detector.direction = in.next(); // the direction of the bombs from batman's current location
                                            // (U, UR, R, DR, D, DL, L or UL)
            System.out.println(detector.findNext());
        }
    }
}
