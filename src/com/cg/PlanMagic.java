package com.cg;

import java.util.Random;

/**
 * Created by sissoko on 15/04/2016.
 */
public class PlanMagic extends Magic<Point, Point> {
    static int MAX_VALUE_X = 10000;
    static int MAX_VALUE_Y = 5000;
    static int MAX_TRIES = 15;
    private int tries = 0;


    public PlanMagic() {
        super(() -> {
            Random random = new Random();
            return new Point(random.nextInt(MAX_VALUE_X), random.nextInt(MAX_VALUE_Y));
        });
    }

    @Override
    public Point guess(Point value) {
        if (++tries >= MAX_TRIES) {
            throw new Error("Maximum tries exceed...");
        }
        int x = 0, y = 0;
        if (magic.x < value.x) {
            x = -1;
        }
        if (magic.x > value.x) {
            x = 1;
        }
        if (magic.y < value.y) {
            y = -1;
        }
        if (magic.y > value.y) {
            y = 1;
        }
        Point p = new Point(x, y);
        return p;
    }
}
