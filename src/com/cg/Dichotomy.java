package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public abstract class Dichotomy<T extends Comparable<T>> {
    Magic magic;

    protected Dichotomy(Magic magic) {
        this.magic = magic;
    }

    public abstract T findMagic();
}
