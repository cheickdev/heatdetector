package com.cg;

import java.util.Random;

/**
 * Created by sissoko on 15/04/2016.
 */
public class LinearMagic extends Magic<Integer, Integer> {

    static int MAX_VALUE = 10000;
    static int MAX_TRIES = 15;
    private int tries = 0;

    public LinearMagic() {
        super(() -> new Random().nextInt(MAX_VALUE));
    }

    @Override
    public Integer guess(Integer value) {
        if (++tries >= MAX_TRIES) {
            throw new Error("Maximum tries exceed...");
        }
        int p = magic.compareTo(value);
        return p;
    }
}
