package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public class PlanDichotomy extends Dichotomy<Point> {

    public PlanDichotomy(Magic magic) {
        super(magic);
    }

    @Override
    public Point findMagic() {
        Point a = new Point(0, 0);
        Point b = new Point(PlanMagic.MAX_VALUE_X, PlanMagic.MAX_VALUE_Y);
        Point guessed = a.mean(b);
        Point position = (Point) magic.guess(guessed);
        while (!position.isNull()) {
            if (position.x < 0) {
                b.x = guessed.x;
            }
            if (position.x > 0) {
                a.x = guessed.x;
            }
            if (position.y < 0) {
                b.y = guessed.y;
            }
            if (position.y > 0) {
                a.y = guessed.y;
            }
            guessed = a.mean(b);
            position = (Point) magic.guess(guessed);
        }
        return guessed;
    }
}
