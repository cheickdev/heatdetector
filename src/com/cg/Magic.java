package com.cg;

/**
 * Created by sissoko on 15/04/2016.
 */
public abstract class Magic<T extends Comparable<T>, V> {

    protected T magic;

    public Magic(MagicGenerator<T> generator) {
        if (generator != null)
            magic = generator.generate();
    }

    public Magic(T magic) {
        this.magic = magic;
    }

    public abstract V guess(T value);
}
